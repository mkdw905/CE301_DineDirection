import supabaseClient from "@/components/supabaseClient";
import { nanoid } from "nanoid";
import 'react-native-get-random-values';
//* References:
//* https://developer.mozilla.org/en/JavaScript_typed_arrays/ArrayBuffer
//* https://developer.mozilla.org/en/JavaScript_typed_arrays/Uint8Array
//* https://github.com/supabase/supabase/issues/7252
//* https://www.reddit.com/r/Supabase/comments/118ef8v/uploading_files_to_storage_with_react_native/

// Base64Binary Class Definition
export const Base64Binary = {
  _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
  decodeArrayBuffer: function(input: string): ArrayBuffer {
    const bytes = (input.length / 4) * 3;
    const ab = new ArrayBuffer(bytes);
    this.decode(input, ab);
    return ab;
  },
  removePaddingChars: function(input: string): string {
    const lkey = this._keyStr.indexOf(input.charAt(input.length - 1));
    if (lkey === 64) {
      return input.substring(0, input.length - 1);
    }
    return input;
  },
  decode: function(input: string, arrayBuffer: ArrayBuffer): Uint8Array {
    input = this.removePaddingChars(input);
    input = this.removePaddingChars(input);

    const bytes = (input.length / 4) * 3;
    const uarray = new Uint8Array(bytes);

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    let j = 0;
    for (let i = 0; i < bytes; i += 3) {
      const enc1 = this._keyStr.indexOf(input.charAt(j++));
      const enc2 = this._keyStr.indexOf(input.charAt(j++));
      const enc3 = this._keyStr.indexOf(input.charAt(j++));
      const enc4 = this._keyStr.indexOf(input.charAt(j++));

      const chr1 = (enc1 << 2) | (enc2 >> 4);
      const chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      const chr3 = ((enc3 & 3) << 6) | enc4;

      uarray[i] = chr1;
      if (enc3 !== 64) uarray[i + 1] = chr2;
      if (enc4 !== 64) uarray[i + 2] = chr3;
    }

    return uarray;
  },
};

// Function to prepare base64 string for image
const prepareBase64DataUrl = (base64: string): string => {
  return base64
    .replace(/^data:image\/[a-zA-Z]+;base64,/, '')
    .trim();
}

// Main Function to upload base64 image to Supabase
const uploadToSupabase = async (
  base64Image: string,
  imageExtension = "jpg",
  bucketName = "BiteSpace"
): Promise<string | null> => {
  try {
    // Prepare the base64 string (remove the 'data:image/*;base64,' part and adjust format)
    const preparedBase64Str = prepareBase64DataUrl(base64Image);
    console.log(base64Image);
    console.log(preparedBase64Str);

    // Decode the base64 string to binary data using Base64Binary.decodeArrayBuffer
    const binaryData = Base64Binary.decodeArrayBuffer(preparedBase64Str);
    console.log(binaryData);

    if (!(binaryData.byteLength > 0)) {
      console.error("[uploadToSupabase] Binary data is empty");
      return null;
    }

    console.log(`[uploadToSupabase] Using bucket: ${bucketName}`);

    // Upload the file to Supabase storage
    const { data, error } = await supabaseClient.storage
      .from(bucketName)
      .upload(`${nanoid()}.${imageExtension}`, binaryData, {
        contentType: `image/${imageExtension}`,
        upsert: true,
      });

    if (error) {
      console.error("[uploadToSupabase] Upload error: ", error.message);
      return null;
    }

    if (!data?.path) {
      console.error("[uploadToSupabase] Upload returned no path");
      return null;
    }

    // Get the public URL for the uploaded image
    const { data: publicUrlData } = supabaseClient.storage
      .from(bucketName)
      .getPublicUrl(data.path);

    if (!publicUrlData?.publicUrl) {
      console.error("[uploadToSupabase] Public URL is null");
      return null;
    }

    return publicUrlData.publicUrl;
  } catch (err) {
    console.error("[uploadToSupabase] Unexpected error: ", err);
    return null;
  }
};


export default uploadToSupabase;
